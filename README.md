## Ingresos

### Requisitos funcionales
- Solo funcionara con ingresos de fecha igual o anterior a un mes
- Debe elaborar, visualizar y aprobar el punto de cuenta
- Debe crear el memorandum
- Debe crear y visualizar el contrato
- Debe crear y llenar el expediente

### Requisitos no funcionales
- Solo puede ser usado por el personal designado.
- Debe tener un desempeño óptimo.
- Necesita Interoperabilidad.
- Los datos se deben almacenar en la base de datos.
- La licencia del código sera de tipo software libre.

### Clases:
- expediente
    - nombres
    - foto (Binary)	
    - cedula
    - nacionalidad
    - edad
    - sexo (Selection)
    - estadocivil (Selection)
    - problema_medico (Boolean)
    - discapacidad (Boolean)
    - nombrar_problema
    - cual_discapacidad
    - telefonos
    - correo
    - fax
    - direccion
    - nivel_estudio
    - institucion
    - titulo
    - ya_trabajo (Boolean)
    - ultimo_empleo
    - ult-fecha_ingreso (Date)
    - ult-fecha_egreso (Date)
    - ult-cargo
    - ult-telefono_trabajo
    - ult-superior
    - curriculum
    - cop_cedula (Boolean)
    - cop_titulo (Boolean)
    - cop_trabajo (Boolean)
    - cop_militar (Boolean)
    - copia_titulo (Binary)
    - constancia_trabajo (Binary)
    - constancia_militar (Binary)
    - copia_cedula (Binary)
    - solicitud_postulacion (Binary)
    - sueldo (Float)
    - fecha_ingreso (Date)
    - fecha_egreso (Date)
    - cargo
    - telefono_trabajo
    - superior
    - incidencias_ids (O2m)


- Incidentes
    - incidente
    - fecha (Date)
    - expediente_id (M2o)

- Punto de cuenta
    - numero
    - remitente
    - dirigido
    - fecha
    - asunto
    - argumento
    - anexos
    - anexs
    - instrucciones
    - state
    - remitente2

- Memorandum
    - titulo
    - remitente
    - dirigido
    - asunto
    - fecha (Date)
    - contenido 

- Contrato
    - 

### Cambios

#### 10/07/2017 - 14/07/2017
- Creada clase **memorandum** junto a sus atributos y su respectiva vista
- Creada clase **contrato**
- Creada clase **expediente** junto a algunos de sus atributos y su respectiva vista
- Creado repositorio

#### 17/07/2017 - 21/07/2017
- Establecido que el expediente solo reciba solicitudes de ingresos aprobadas
- Creada clase **puntoCuenta** junto a sus atributos y su respectiva vista
- Espediente se trae todos los campos tanto de la solicitud de ingreso como del postulado (documentos)
- Creada clase **Incidencias**
- Añadido el sueldo como monetario
- Añadido diagrama de clases
- Añadido clase **Contrato**
- Creada clase **Clausulas**
- Establecidos campos M2o como selection
- Añadida clase **Masclausulas** para las clausulas adicionales del contrato
- Varios campos establecidos como obligatorios
- Añadida vista kanban en **expedientes**
